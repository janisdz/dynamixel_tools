# Wireshark dissector for Dynamixel #

This is a Wireshark dissector for Dynamixel protocol.

Add it to your plugins directory and you should be able to see your communications with dynamixels.

It automatically binds to TCP ports 4002 and 4003 as those are the ones I'm using in my setup.
It also binds heuristic dissector to USB if you want to sniff directly from USB2Dynamixel device.

Verified with Wireshark 1.10.6 on Ubuntu.

