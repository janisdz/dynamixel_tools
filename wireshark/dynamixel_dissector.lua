-- Wireshark dissector for the Dynamixel protocol 1.0
--
-- Usage: Copy or link this file into your wireshark plugins directory.
--        Detailed instructions here: http://wiki.wireshark.org/Lua
--
-- Author: Janis Dzerve

dynamixel_proto = Proto("dynamixel", "Dynamixel protocol")
local f = dynamixel_proto.fields
local COMMANDS = {[1]="PING", [2]="READ", [3]="WRITE", [4]="REG_WRITE", [5]="ACTION", [6]="RESET", [131]="SYNC_WRITE", [146]="BULK_READ"}

local funs = {}

f.length = ProtoField.uint8("dynamixel.length", "Data length")
f.id = ProtoField.uint8("dynamixel.id",         "Servo ID   ")
f.cmd = ProtoField.uint8("dynamixel.cmd",       "Command    ", base.DEC, COMMANDS)
f.err = ProtoField.uint8("dynamixel.err",       "Error      ", base.HEX)
f.param = ProtoField.uint8("dynamixel.param",   "Parameter  ", base.DEC)
f.param16 = ProtoField.int16("dynamixel.param16",   "Parameter16", base.DEC)
f.csum = ProtoField.uint8("dynamixel.csum",     "Checksum   ", base.HEX)
f.addr = ProtoField.uint8("dynamixel.addr",     "Address    ", base.DEC)
f.value = ProtoField.uint8("dynamixel.value",   "Value      ", base.DEC)
f.count = ProtoField.uint8("dynamixel.count",   "Count      ", base.DEC)
f.err_v = ProtoField.uint8("dynamixel.err_volt",  "Voltage error  ", base.DEC, nil, 0x01)
f.err_a = ProtoField.uint8("dynamixel.err_angle", "Angle error    ", base.DEC, nil, 0x02)
f.err_h = ProtoField.uint8("dynamixel.err_heat",  "Overheat error ", base.DEC, nil, 0x04)
f.err_r = ProtoField.uint8("dynamixel.err_range", "Range error    ", base.DEC, nil, 0x08)
f.err_c = ProtoField.uint8("dynamixel.err_csum",  "Csum error     ", base.DEC, nil, 0x10)
f.err_o = ProtoField.uint8("dynamixel.err_overl", "Overload error ", base.DEC, nil, 0x20)
f.err_i = ProtoField.uint8("dynamixel.err_instr", "Instruction err", base.DEC, nil, 0x40)

-- dissector function
function dynamixel_proto.dissector(buffer, pinfo, tree)
    local offset = 0
    local frame_count = 0
            
    while offset < buffer:len() do

        -- skip until we have a preamble
	    while (offset < buffer:len() - 1) do
	        if (buffer(offset, 2):uint() == 0xffff) then
	            break
	        end
	        offset = offset + 1
	    end

        -- we need at least 6 bytes to start dissecting.
        local bytes_remaining = buffer:len() - offset
        if (bytes_remaining < 6) then
            pinfo.desegment_offset = offset
            pinfo.desegment_len = 6 - bytes_remaining
            return;
        end 

        local length = buffer(offset + 3, 1)
        payload_size = length:uint()
        
        if (payload_size + 4 > bytes_remaining) then
            -- buffer does not contain all the packet data we need.
            pinfo.desegment_offset = offset
            pinfo.desegment_len = payload_size + 4 - bytes_remaining
            return;
        end

        -- calc checksum
        local csum = buffer(offset + payload_size + 3, 1)
        local csum_received = csum:uint()
        local csum_calced = 0

        for i = offset + 2, offset + payload_size + 2 do
            csum_calced = csum_calced + buffer(i, 1):uint()
        end
        csum_calced = bit.bnot(csum_calced)
        csum_calced = bit.band(csum_calced, 0xff)
        
        if (csum_calced == csum_received) then
            -- Checksum is fine, proceed with dissection
            local id = buffer(offset + 2, 1)
            subtree = tree:add(dynamixel_proto, buffer(offset, payload_size + 4), "DXL servo " .. id:uint())
            
            local command = buffer(offset + 4, 1)
            local cmd = command:uint();

            subtree:add(f.id, id)
            subtree:add(f.length, length)
            
            -- There is no way to distinguish between requests and responses based on a single packet data.
            -- The 4th byte is either error code or command id.
            -- Assume that if it contains valid command then it is a request.
            -- Good enough for our purposes.

            if (COMMANDS[cmd] == nil) then
                -- This does not corespond to any command. Must be a response.
                subtree:add(f.err, command)
-- Uncomment this if you want errors expanded
--              errtree = subtree:add(dynamixel_proto, buffer(offset + 4, 1), "Error" .. command)
--              errtree:add(f.err_v, command)
--              errtree:add(f.err_a, command)
--              errtree:add(f.err_h, command)
--              errtree:add(f.err_r, command)
--              errtree:add(f.err_c, command)
--              errtree:add(f.err_o, command)
--              errtree:add(f.err_i, command)

                -- Add data bytes to the tree
                for i = offset + 5, offset + payload_size + 2 do
                    subtree:add(f.param, buffer(i, 1))
                end

                -- Add data interpreted as int16
                for i = offset + 5, offset + payload_size + 1, 2 do
                    subtree:add_le(f.param16, buffer(i, 2))
                end
            else
                subtree:add(f.cmd, command)
                if (command:uint() == 1) then
                    -- PING
                elseif (command:uint() == 2) then
                    -- READ
                    subtree:add(f.addr, buffer(offset + 5, 1))
                    subtree:add(f.count, buffer(offset + 6, 1))
                elseif (command:uint() == 3) or (command:uint() == 4) then
                    -- WRITE
                    -- REG_WRITE
                    subtree:add(f.addr, buffer(offset + 5, 1))
                    for i = offset + 6, offset + payload_size + 1 do
                        subtree:add(f.value, buffer(i, 1))
                    end
                elseif (command:uint() == 5) then
                    -- ACTION
                elseif (command:uint() == 6) then
                    -- RESET
                elseif (command:uint() == 131) then
                    -- SYNC_WRITE
                    subtree:add(f.addr, buffer(offset + 5, 1))
                    local len = buffer(offset + 6, 1):uint()
                    subtree:add(f.count, buffer(offset + 6, 1))
                    for i = offset + 7, offset + payload_size, len do
                        subtree:add(f.id, buffer(i, 1))
                        for j = i, i + count - 1 do
                            subtree:add(f.value, buffer(j, 1))
                        end
                    end
                elseif (command:uint() == 146) then
                    -- BULK_READ
                    subtree:add(f.param, buffer(offset + 5, 1))
                    local len = buffer(offset + 6, 1):uint()
                    for i = offset + 7, offset + payload_size, 3 do
                        subtree:add(f.count, buffer(i, 1))
                        subtree:add(f.id, buffer(i + 1, 1))
                        subtree:add(f.addr, buffer(i + 2, 1))
                    end
                end
            end
            
            offset = offset + payload_size + 4
            frame_count = frame_count + 1
        else
            -- bad checksum. Retry parser from next byte.
            offset = offset + 1
        end
    end

    -- some Wireshark decoration
    if (frame_count) then
        pinfo.cols.protocol = "dynamixel"
        pinfo.cols.info = "DXL frames: " .. frame_count
    end
    return offset
end

-- bind protocol dissector to some TCP ports
tcp_encap = DissectorTable.get("tcp.port")
tcp_encap:add(4002, dynamixel_proto)
tcp_encap:add(4003, dynamixel_proto)

-- bind it to usb, too.
usb_encap = DissectorTable.get("usb.bulk")
usb_encap:add(0xffff, dynamixel_proto)

